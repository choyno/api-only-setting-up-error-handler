class ApplicationController < ActionController::Base
  rescue_from Exception do |e|
    Rails.logger.error(e)
    Rails.logger.error(e.backtrace.join("\n"))
    render json:   { message: "There was a problem. Please try again later." },
           status: :internal_server_error
  end
end
