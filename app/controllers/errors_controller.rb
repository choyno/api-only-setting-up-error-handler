class ErrorsController < ActionController::API
  include ApiErrorHandler

  def render_404
    raise ErrorList::NotFound, "The resource was not found."
  end
end
