class Api::V1::PostsController < Api::V1::BaseController
  api :GET, "/posts", "Posts List"
  validates :index do
    integer :page, required: true, strong: true
  end
  def index
    @sample = { name: "post 1 should get this from db and loop this result" }
    # render json: { result: [{ name: "post 1"}] }, status: 200
  end

  validates :create do
    string :title, required: true, strong: true
    string :description, required: true, strong: true
  end
  def create; end
end
