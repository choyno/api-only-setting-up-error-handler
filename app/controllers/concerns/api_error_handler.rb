module ApiErrorHandler
  extend ActiveSupport::Concern

  included do
    rescue_from WeakParameters::ValidationError do |e|
      Rails.logger.error(e)
      Rails.logger.error(e.backtrace.join("\n"))
      render json: { message: e.message }, status: :bad_request
    end

    rescue_from ActionController::ParameterMissing do |e|
      Rails.logger.error(e)
      Rails.logger.error(e.backtrace.join("\n"))
      render json: { message: e.message }, status: :bad_request
    end

    rescue_from ErrorList::NotFound do |e|
      render json: { message: e.message }, status: :not_found
    end
  end
end
