module ErrorList
  class Unauthorized < StandardError; end

  class BadGateway < StandardError; end

  class BadRequest < StandardError; end

  class Conflict < StandardError; end

  class GatewayTimeout < StandardError; end

  class InvalidSignature < StandardError; end

  class NotFound < StandardError; end

  class RateLimitExceeded < StandardError; end

  class ServiceUnavailable < StandardError; end

  class TooManyRequests < StandardError; end
end
