Rails.application.routes.draw do
  # url http://localhost:3000/apipie
  apipie

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'errors#render_404'

  namespace :api, defaults: { format: 'json' } do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :posts, only: [:index, :create], param: :slug
    end
  end

  delete '*path', controller: 'errors', action: 'render_404'
  get '*path', controller: 'errors', action: 'render_404'
  patch '*path', controller: 'errors', action: 'render_404'
  post '*path', controller: 'errors', action: 'render_404'
  put '*path', controller: 'errors', action: 'render_404'

end
