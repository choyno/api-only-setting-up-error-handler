Rails.application.configure do
  config.lograge.enabled = true
  config.lograge.base_controller_class = ['ActionController::API', 'ActionController::Base']

  config.lograge.custom_options = lambda do |event|
    exceptions = %w[controller action format id]
    {
      time:             Time.zone.now,
      host:             event.payload[:host],
      remote_ip:        event.payload[:remote_ip],
      params:           event.payload[:params].except(*exceptions),
      exception_object: event.payload[:exception_object],
      exception:        event.payload[:exception],
      backtrace:        event.payload[:exception_object].try(:backtrace)
    }
  end
end
